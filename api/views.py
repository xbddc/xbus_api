#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import urllib2
import json


TAG_LIST = 'tags_list.json'
TAGS = "tags/"

def load_data(fname):
    json_data = open(fname)
    return json.load(json_data)

def save_data(fname, json_object):
    with open(fname.encode('utf-8', 'ignore'), 'w') as json_file:
        json.dump(json_object, json_file, sort_keys = True, indent = 4)


tag_sets = load_data(TAG_LIST)

def get_tags(request):
    response = []
    for tag_name, tag_data in tag_sets.items():
        icon_type = 0
        if 'icon_type' in tag_data:
            icon_type = tag_data["icon_type"]
        response.append({"tag_name": tag_name, "poi_counts":tag_data['poi_counts'], "icon_type": icon_type})
    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=True))


def get_tag_data(request):
    response = {}
    tag_name = request.GET.get('tag_name', u"軍")
    global tag_sets
    return HttpResponse(json.dumps(tag_sets[tag_name], indent = 4, ensure_ascii=True))


def search_tag(request):
    search_hit = []
    search_tag = request.GET.get('search_tag', u"軍").encode('utf-8').rstrip()
    start = request.GET.get('start', u"0").encode('utf-8').rstrip()
    fq = request.GET.get('fq', u"").encode('utf-8').rstrip()
    response = {'tag_name': search_tag}
    q = urllib2.Request('http://localhost:8983/solr/core/select', 'q=cht:*%s*&wt=json&start=%s&rows=500&fq=%s' % (search_tag, start, fq))
    p = urllib2.urlopen(q)
    j = json.loads(p.read())
    p.close()
    search_hit = j['response']['docs']
    response['poi_counts'] = j['response']['numFound']
    response['poi'] = search_hit
    response['icon_type'] = 0
    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=True))


@csrf_exempt
def save_tag(request):
    # curl -H "Content-Type: application/json" -d '{"username":"xyz","password":"xyz"}' http://localhost:3000/api/login
    #response = {}
    #return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=False).encode("utf-8"))
    #if request.is_ajax():
    if request.method == 'POST':
        #print 'Raw Data: "%s"' % request.body
        json_object = json.loads(request.body)
        #print type(json_object)
        if 'tag_name' not in json_object:
            return HttpResponse("1")
        #filename = TAGS + json_object['tag_name'] + ".json"
        #save_data(filename, json_object)
        global tag_sets
        #print "~"
        json_object['poi'] = None
        tag_sets[json_object['tag_name']] = json_object
        save_data(TAG_LIST, tag_sets)
        return HttpResponse("0")
    return HttpResponse("1")


@csrf_exempt
def save_search_result(request):
    return save_tag(request)

def delete_tag(request):
    try:
        tag = request.GET.get('tag_name', u"軍")
        #print "delete tag" + tag
        global tag_sets
        tag_sets.pop(tag, None)
        save_data(TAG_LIST, tag_sets)
        return HttpResponse("0")
    except Exception,e: 
        print str(e)
        print "ERROR"
        return HttpResponse("1")
    return HttpResponse("1")
