from django.conf.urls import url

from api import views

urlpatterns = [
    url(r'^_get_tags$', views.get_tags),
    url(r'^_get_tag_data$', views.get_tag_data),
    url(r'^_search_tag$', views.search_tag),
    url(r'^_save_tag$', views.save_tag),
    url(r'^_delete_tag$', views.delete_tag),
    url(r'^_save_search_result$', views.save_search_result),
    url(r'^_delete_tag$', views.delete_tag),
]
