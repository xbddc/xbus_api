Django==1.6.5
PyMySQL==0.6.2
argparse==1.2.1
django-cors-headers==0.12
ipython==2.1.0
pysolr==3.2.0
requests==2.3.0
wsgiref==0.1.2
